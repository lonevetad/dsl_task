package persons.tasks.scoping;

import java.util.Collection;
import java.util.LinkedList;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import persons.tasks.scoping.AbstractTaskDSLScopeProvider;
import persons.tasks.taskDSL.Task;

/**
 * This class contains custom scoping description.
 * 
 * See
 * https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
@SuppressWarnings("all")
public class TaskDSLScopeProvider extends AbstractTaskDSLScopeProvider {
  public IScope scope_ProjectUse_project(final Task task, final EReference ref) {
    IScope scope = IScope.NULLSCOPE;
    Collection<Task> visited = new LinkedList<Task>();
    Task runningTask = task;
    while (((runningTask != null) && (!visited.contains(runningTask)))) {
      {
        visited.add(runningTask);
        scope = Scopes.scopeFor(runningTask.getProjects(), scope);
        runningTask = runningTask.getExtends();
      }
    }
    return scope;
  }
}
