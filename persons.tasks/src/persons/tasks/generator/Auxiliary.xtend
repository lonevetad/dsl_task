package persons.tasks.generator

import java.util.List
import persons.tasks.taskDSL.Action
import persons.tasks.taskDSL.Person
import persons.tasks.taskDSL.Planning

class Auxiliary {
	def static List<Action> getActions(Planning root) {
//		var List<Action> actionlist = new ArrayList<Action>(root.tasks.size)
//		for (Task t : root.tasks){
//			actionlist.add(t.action)
//		}
//		return actionlist;
		return root.tasks.map[t|t.action]
	}
	
	def static Planning getPersonPlanning(Person person) {
		return person.eContainer() as Planning
	}
}