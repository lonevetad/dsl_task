package persons.tasks.scoping

import java.util.Collection
import org.eclipse.xtext.scoping.IScope
import persons.tasks.taskDSL.Task
import org.eclipse.emf.ecore.EReference
import java.util.LinkedList
import org.eclipse.xtext.scoping.Scopes


/**
 * This class contains custom scoping description.
 * 
 * See
 * https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class TaskDSLScopeProvider extends AbstractTaskDSLScopeProvider {
	def IScope scope_ProjectUse_project(Task task, EReference ref) {
		var IScope scope = IScope.NULLSCOPE;
		var Collection<Task> visited = new LinkedList<Task>();
		var runningTask = task;
		while (runningTask !== null && !visited.contains(runningTask)) {
			visited.add(runningTask);
			scope = Scopes.scopeFor(runningTask.getProjects(), scope);
			runningTask = runningTask.getExtends();
		}
		return scope;
	}
}