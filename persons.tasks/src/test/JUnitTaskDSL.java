package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import persons.tasks.validation.TaskDSLValidator;

class JUnitTaskDSL {

	static Function<String, String> stringIdentity;
	static Comparator<String> stringComparator;
	static Consumer<? super PairOf<Integer, String>> pairPrinter;

	@BeforeAll
	static void setUp() throws Exception {
		stringIdentity = s -> s;
		stringComparator = String::compareTo;
		pairPrinter = p -> System.out.println("\t- " + p.element1 + " , at: " + p.element2);;
	}

	void testGeneric(String[] examplesArray, int expectedDuplicates) {
		ListOfExamples examples;
		Map<String, Integer> presenceTracker;
		TaskDSLErrorCollector errorCollector;
		presenceTracker = new TreeMap<>(stringComparator);
		errorCollector = new TaskDSLErrorCollector();
		examples = new ListOfExamples(examplesArray.length);
		for (String ex : examplesArray)
			examples.add(ex);

		errorCollector.pinpointDuplicate(examples, null, presenceTracker, stringIdentity,
				"test_" + expectedDuplicates + "_Duplicate_");
		if (errorCollector.indexesDuplication.size() != expectedDuplicates) {
			System.out.println("\n\nERROR: on " + examplesArray.length + " examples: expected: " + expectedDuplicates
					+ ", got :" + errorCollector.indexesDuplication.size());
			errorCollector.indexesDuplication.forEach(pairPrinter);
		}
		assertEquals(errorCollector.indexesDuplication.size(), expectedDuplicates);
	}

	@Test
	void testNoDuplicate_empty() { testGeneric(new String[] {}, 0); }

	@Test
	void testNoDuplicate_one() { testGeneric(new String[] { "Mario" }, 0); }

	@Test
	void testNoDuplicate_two() { testGeneric(new String[] { "Mamma", "mia" }, 0); }

	@Test
	void testNoDuplicate_three() { testGeneric(new String[] { "It's", "a", "me" }, 0); }

	@Test
	void testNoDuplicate_many() { testGeneric(new String[] { "one", "two", "three", "four", "five" }, 0); }

	@Test
	void testDuplicate_onTwo() { testGeneric(new String[] { "test", "test" }, 1); }

	@Test
	void testDuplicate_All_few() { testGeneric(new String[] { "Something", "strange", "strange", "Something" }, 2); }

	@Test
	void testDuplicate_Many() {
		testGeneric(new String[] { //
				"I", "would", "say", "something", "if", "I", "would", "have", "something", "to", "say", "but", "if",
				"something", "is", "interesting", "I", "'ll", "say", "it" }, //
				8);
	}

	//

	//

	//

	static class TaskDSLErrorCollector extends TaskDSLValidator {
		/** Pairs of (last) indexes and duplicated item's name */
		List<PairOf<Integer, String>> indexesDuplication = new LinkedList<>();

		@Override
		protected void error(String message, EStructuralFeature feature, int index) {
			indexesDuplication.add(new PairOf<>(index, message));
		}
	}

	protected static class PairOf<E, T> {
		protected E element1;
		protected T element2;

		protected PairOf(E element1, T element2) {
			this.element1 = element1;
			this.element2 = element2;
		}
	}

	static class ListOfExamples extends ArrayList<String> implements EList<String> {
		private static final long serialVersionUID = 1L;

		public ListOfExamples() { super(); }

		public ListOfExamples(int initialCapacity) { super(initialCapacity); }

		@Override
		public void move(int newPosition, String object) {
			int indexObj = indexOf(object);
			if (indexObj < 0) return;
			move(newPosition, indexObj);
		}

		@Override
		public String move(int newPosition, int oldPosition) {
			if (newPosition > oldPosition) {
				int t = newPosition;
				newPosition = oldPosition;
				oldPosition = t;
			}

			return null;
		}
	}
	/*
	 * static class ListOfExamples extends LinkedList<String> implements
	 * EList<String> { private static final long serialVersionUID = 1L;
	 * 
	 * @Override public void move(int newPosition, String object) { int indexObj =
	 * indexOf(object); if (indexObj < 0) return; move(newPosition, indexObj); }
	 * 
	 * @Override public String move(int newPosition, int oldPosition) { if
	 * (newPosition > oldPosition) { int t = newPosition; newPosition = oldPosition;
	 * oldPosition = t; } // nope, not needed return null; } }
	 */
}